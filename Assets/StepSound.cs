using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSound : MonoBehaviour
{
    public AudioClip step_sound_1;
    public AudioClip step_sound_2;
    private bool toggle;
    private AudioSource sounds;

    // Start is called before the first frame update
    void Start()
    {
        toggle=false;
        sounds = GetComponent<AudioSource>();
        Debug.Log("Sonido asociado "+ sounds.clip);
    }

    void playStep()
    {
        if(toggle){
            sounds.PlayOneShot(step_sound_1);
            toggle=false;
        }else{
            sounds.PlayOneShot(step_sound_2);
            toggle=true;
        }

    }
    // Update is called once per frame
    void Update()
    {
    }

}
