using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaAtaque : MonoBehaviour
{
    public Animator jugadorAnim;
    public ControlPersonaje jugadorScript;
    public GameObject colliderAtaque;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetButtonDown("Fire1") && jugadorScript.estaEnElSuelo)
        if (Input.GetButtonDown("Fire1"))
        {
            //Debug.Log("Ataque");
            jugadorAnim.SetTrigger("pega");
        }
    }

    public void AtacaTrue()
    {
        jugadorScript.puedeMover = false;
        colliderAtaque.SetActive(true);
    }
    public void AtacaFalse()
    {
        jugadorScript.puedeMover = true;
        colliderAtaque.SetActive(false);
    }
}
