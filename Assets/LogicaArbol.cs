using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogicaArbol : MonoBehaviour
{
    private static int numArboles = 13;
    public Text TXTscore;
    public AudioSource golpeArbol;
    public AudioSource caidaArbol;
    public float vidaObjeto;
    public float danio;
    float vida;
    // Start is called before the first frame update
    void Start()
    {
        vida = vidaObjeto;
        TXTscore.text = numArboles.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (vida == 0)
        {
            Destroy(gameObject,1);
            numArboles--;
            Debug.Log($"Arboles restantes: {numArboles}");
        }
        */
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("AtaqueJugador"))
        {
            //Debug.Log("golpe al arbol");
            vida -= danio;
            golpeArbol.Play();
            if (vida == 0)
            {
                FindObjectOfType<Informacion>().muestraInfo();
                Destroy(gameObject, 0.5f);
                numArboles--;
                caidaArbol.Play();
                TXTscore.text = numArboles.ToString();
                Debug.Log($"Arboles restantes: {numArboles}");
            }
        }
    }
}
