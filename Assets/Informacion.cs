using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Informacion : MonoBehaviour
{
    private Queue<string> info;
    public Dialogo dialogo;
    public TextMeshProUGUI vText;

    void Start()
    {
        info = new Queue<string>();
        foreach (string linea in dialogo.renglon)
        {
            info.Enqueue(linea);
        }
    }

    public void muestraInfo()
    {
        Debug.Log("Muestra la informacion");

        //info.Clear();
        if(info.Count == 0)
        {
            finInfo();
            return;
        }

        string informacion = info.Dequeue();
        vText.text = informacion;
        Debug.Log(informacion);
        
    }
    public void finInfo()
    {
        Debug.Log("Fin de la informacion");
    }
}
