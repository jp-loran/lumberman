using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayLumberMan()
    {
        Debug.Log("LoadSceneBosque");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void fin()
    {
        Debug.Log("Fin");
        Application.Quit();
    }
}
